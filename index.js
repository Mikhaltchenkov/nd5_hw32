const express = require('express');
const app = express();
const bodyParser = require('body-parser');

const api = require('./api');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({'extended':true}));

app.use('/api', api);

app.listen(3000,() => {
   console.log('Server started on port 3000');
});
