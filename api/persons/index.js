const model = require('../../models/persons');
const modelContacts = require('../../models/contacts');
const express = require('express');
const app = module.exports = express();

app.get('/', (req, res) => {
  model.list(req.query.field, req.query.value, (err, result) => {
    if (err) {
      res.status(err.code);
      res.json({'message': err.message});
    } else {
      res.json({'persons': result});
    }
  });
});

app.post('/', (req, res) => {
  if (req.body.name && req.body.surname) {
    model.add(req.body.name, req.body.surname, (err, result) => {
      if (err) {
        res.status(err.code);
        res.json({'message': err.message});
      } else {
        res.json({'persons': result});
      }
    });
  } else {
    res.status(400);
    res.json({'message': 'Error: Please fill fields \'name\' and \'surname\'!'});
  }
});

app.put('/:id', (req, res) => {
  if (req.body.name && req.body.surname) {
    model.change(req.params.id, req.body.name, req.body.surname, (err, result) => {
      if (err) {
        res.status(err.code);
        res.json({'message': err.message});
      } else {
        res.json({'persons': result});
      }
    });
  } else {
    res.status(400);
    res.json({'message': 'Error: Please fill fields \'name\' and \'surname\'!'});
  }
});


app.get('/:id', (req, res) => {
  model.get(req.params.id, (err, result) => {
    if (err) {
      res.status(err.code);
      res.json({'message': err.message});
    } else {
      res.json({'persons': result});
    }
  });
});

app.delete('/:id', (req, res) => {
  model.remove(req.params.id, (err, result) => {
    if (err) {
      res.status(err.code);
      res.json({'message': err.message});
    } else {
      res.status(204);
      res.json({'message': 'OK'});
    }
  });
});

// Get contacts of person Id
app.get('/:id/contacts', (req, res) => {
  modelContacts.list(req.params.id, (err, result) => {
    if (err) {
      res.status(err.code);
      res.json({'message': err.message});
    } else {
      res.json({'contacts': result});
    }
  });
});

// Post contacts to person Id
app.post('/:id/contacts', (req, res) => {
  if (req.params.id && req.body.phone) {
    modelContacts.add(req.params.id, req.body.phone, (err, result) => {
      if (err) {
        res.status(err.code);
        res.json({'message': err.message});
      } else {
        res.json({'message': result});
      }
    });
  } else {
    res.status(400);
    res.json({'message': 'Error: Please fill fields \'type\' and \'value\'!'});
  }
});

// Delete contacts from person
app.delete('/:id/contacts/:phone', (req, res) => {
  modelContacts.remove(req.params.id, req.params.phone, (err, result) => {
    if (err) {
      res.status(err.code);
      res.json({'message': err.message});
    } else {
      res.status(204);
      res.json({'message': 'OK'});
    }
  });
});
