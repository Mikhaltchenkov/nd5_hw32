const mongodb = require('mongodb');
const mc = mongodb.MongoClient;
const url = 'mongodb://127.0.0.1:27017/nd5_hw32';

exports.list = function(personId, cb) {
  console.log(`[contacts.list]: personId: ${personId} retrieving...`);
  mc.connect(url, (err, db) => {
    if (err) {
      cb({'code': 500, 'message': 'Can\'t connect to MongoDB. Error:' + err});
    } else {
      db.collection('persons').findOne({'_id': new mongodb.ObjectID(personId)}, {'contacts': true}, (err, person) => {
        if (err) {
          console.log(`[contacts.list]: Error: ${err}`);
          cb({'code': 500, 'message': 'Error retrieving person info: ' + err});
        } else {
          console.log(`[contacts.list]: OK`);
          cb(null, person.contacts);
        }
      });
      db.close();
    }
  });
};

exports.add = function(personId, phone, cb) {
  console.log(`[contacts.list]: personId: ${personId} retrieving...`);
  mc.connect(url, (err, db) => {
    if (err) {
      cb({'code': 500, 'message': 'Can\'t connect to MongoDB. Error:' + err});
    } else {
      /// Закончить
      db.collection('persons').update({'_id': new mongodb.ObjectID(personId)}, {$addToSet: {'contacts': phone }}, (err, person) => {
        if (err) {
          console.log(`[contacts.add]: Error: ${err}`);
          cb({'code': 500, 'message': 'Error retrieving person info: ' + err});
        } else {
          console.log(`[contacts.add]: OK`);
          cb(null, 'OK');
        }
      });
    }
  });
};

exports.remove = function(id, phone, cb) {
  console.log(`[remove]: id = ${id}, phone = ${phone}`);
  mc.connect(url, (err, db) => {
    if (err) {
      cb({'code': 500, 'message': 'Can\'t connect to MongoDB. Error:' + err});
    } else {
      db.collection('persons').update({'_id': new mongodb.ObjectID(id)}, {$pull: { 'contacts': phone }}, (err, result) => {
        if (err) {
          console.log(`[contacts.remove]: Error: ${err}`);
          cb({'code': 500, 'message': 'Error retrieving person info: ' + err});
        } else {
          console.log(`[contacts.remove]: OK`);
          cb(null, 'OK');
        }
      });
    }
  });
};
