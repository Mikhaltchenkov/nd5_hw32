const mongodb = require('mongodb');
const mc = mongodb.MongoClient;
const url = 'mongodb://127.0.0.1:27017/nd5_hw32';

exports.get = function(id, cb) {
  console.log(`[get]: id = ${id}`);
  mc.connect(url, (err, db) => {
    if (err) {
      cb({'code': 500, 'message': 'Can\'t connect to MongoDB. Error:' + err});
    } else {
      db.collection('persons').find({'_id': new mongodb.ObjectID(id)}).toArray((err, result) => {
        db.close();
        if (err) {
          cb({'code': 500, 'message': 'Error:' + err});
        } else {
          cb(null, result);
        }
      });
    }
  });
};

exports.list = function(field, value, cb) {
  console.log('[list]');
  mc.connect(url, (err, db) => {
    if (err) {
      cb({'code': 500, 'message': 'Can\'t connect to MongoDB. Error:' + err});
    } else {
      let findObj = {};
      if (field && value) {
        console.log(`[list] field: ${field}, string: ${value}`);
        findObj[field] = new RegExp(value, 'i');
      }
      db.collection('persons').find(findObj).toArray((err, result) => {
        db.close();
        if (err) {
          cb({'code': 500, 'message': 'Error:' + err});
        } else {
          cb(null, result);
        }
      });
    }
  });
};

exports.add = function(name, surname, cb) {
    mc.connect(url, (err, db) => {
      if (err) {
        cb({'code': 500, 'message': 'Can\'t connect to MongoDB. Error:' + err});
      } else {
        db.collection('persons').insert({'name': name, 'surname': surname, 'contacts':[]}, (err, result) => {
          try {
            if (err) {
              cb({'code': 500, 'message': 'Error:' + err});
            } else {
              db.collection('persons').find({'name': name, 'surname': surname}).toArray((err, result) => {
                if (err) {
                  cb({'code': 500, 'message': 'Error:' + err});
                } else {
                  cb(null, result);
                }
              });
            }
          }
          finally {
            db.close();
          }
        });
      }
    });
};

exports.change = function(id, name, surname, cb) {
  console.log(`[change]: id = ${id}, name = ${name}, surname: ${surname}`);
  mc.connect(url, (err, db) => {
    if (err) {
      cb({'code': 500, 'message': 'Can\'t connect to MongoDB. Error:' + err});
    } else {
      let objectId = new mongodb.ObjectID(id);
      db.collection('persons').update({'_id': objectId}, {'name': name, 'surname': surname});

      db.collection('persons').find({'_id': objectId}).toArray((err, result) => {
        db.close();
        if (err) {
          cb({'code': 500, 'message': 'Error:' + err});
        } else {
          cb(null, result);
        }
      });
    }
  });
};

exports.remove = function(id, cb) {
  console.log(`[remove]: id = ${id}`);
  mc.connect(url, (err, db) => {
    if (err) {
      cb({'code': 500, 'message': 'Can\'t connect to MongoDB. Error:' + err});
    } else {
      db.collection('persons').remove({'_id': new mongodb.ObjectID(id)}, {justOne: true}, (err, result) => {
        if (err) {
          cb({'code': 500, 'message': 'Error:' + err});
        } else {
          cb(null, result);
        }
      });
    }
  });
};
